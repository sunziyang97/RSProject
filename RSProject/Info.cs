﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RSProject
{
    public partial class Info : Form
    {
        public Info()
        {
            StartPosition = FormStartPosition.CenterParent; ;
            InitializeComponent();
        }

        Head head1 = Global.head;

        private void Info_Load(object sender, EventArgs e)
        {
            txtSample.Text = head1.samples.ToString();
            txtLine.Text = head1.lines.ToString();
            txtBand.Text = head1.bands.ToString();
            txtOffset.Text = head1.header_offset.ToString();
            txtDataType.Text = head1.data_type.ToString();
            txtFileType.Text = head1.file_type;
            txtInter.Text = head1.interleave;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            head1.samples = int.Parse(txtSample.Text);
            head1.lines = int.Parse(txtLine.Text);
            head1.bands = int.Parse(txtBand.Text);
            head1.header_offset = int.Parse(txtOffset.Text);
            head1.file_type = txtFileType.Text;
            head1.data_type = int.Parse(txtDataType.Text);
            head1.interleave = txtInter.Text;
            Global.head = head1;
        }
    }
}
