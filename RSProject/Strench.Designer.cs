﻿namespace RSProject
{
    partial class Strench
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtLeft0 = new System.Windows.Forms.TextBox();
            this.lblLeft0 = new System.Windows.Forms.Label();
            this.lblRight0 = new System.Windows.Forms.Label();
            this.txtRight0 = new System.Windows.Forms.TextBox();
            this.lblLeft1 = new System.Windows.Forms.Label();
            this.lblRight1 = new System.Windows.Forms.Label();
            this.txtLeft1 = new System.Windows.Forms.TextBox();
            this.txtRight1 = new System.Windows.Forms.TextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtLeft0
            // 
            this.txtLeft0.Location = new System.Drawing.Point(124, 9);
            this.txtLeft0.Name = "txtLeft0";
            this.txtLeft0.Size = new System.Drawing.Size(82, 22);
            this.txtLeft0.TabIndex = 0;
            // 
            // lblLeft0
            // 
            this.lblLeft0.AutoSize = true;
            this.lblLeft0.Location = new System.Drawing.Point(12, 9);
            this.lblLeft0.Name = "lblLeft0";
            this.lblLeft0.Size = new System.Drawing.Size(106, 17);
            this.lblLeft0.TabIndex = 1;
            this.lblLeft0.Text = "待拉伸区间左侧";
            // 
            // lblRight0
            // 
            this.lblRight0.AutoSize = true;
            this.lblRight0.Location = new System.Drawing.Point(12, 39);
            this.lblRight0.Name = "lblRight0";
            this.lblRight0.Size = new System.Drawing.Size(106, 17);
            this.lblRight0.TabIndex = 2;
            this.lblRight0.Text = "待拉伸区间右侧";
            // 
            // txtRight0
            // 
            this.txtRight0.Location = new System.Drawing.Point(124, 39);
            this.txtRight0.Name = "txtRight0";
            this.txtRight0.Size = new System.Drawing.Size(82, 22);
            this.txtRight0.TabIndex = 3;
            // 
            // lblLeft1
            // 
            this.lblLeft1.AutoSize = true;
            this.lblLeft1.Location = new System.Drawing.Point(229, 9);
            this.lblLeft1.Name = "lblLeft1";
            this.lblLeft1.Size = new System.Drawing.Size(106, 17);
            this.lblLeft1.TabIndex = 4;
            this.lblLeft1.Text = "拉伸至区间左侧";
            // 
            // lblRight1
            // 
            this.lblRight1.AutoSize = true;
            this.lblRight1.Location = new System.Drawing.Point(229, 39);
            this.lblRight1.Name = "lblRight1";
            this.lblRight1.Size = new System.Drawing.Size(106, 17);
            this.lblRight1.TabIndex = 5;
            this.lblRight1.Text = "拉伸至区间右侧";
            // 
            // txtLeft1
            // 
            this.txtLeft1.Location = new System.Drawing.Point(341, 6);
            this.txtLeft1.Name = "txtLeft1";
            this.txtLeft1.Size = new System.Drawing.Size(82, 22);
            this.txtLeft1.TabIndex = 6;
            // 
            // txtRight1
            // 
            this.txtRight1.Location = new System.Drawing.Point(341, 39);
            this.txtRight1.Name = "txtRight1";
            this.txtRight1.Size = new System.Drawing.Size(82, 22);
            this.txtRight1.TabIndex = 7;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(341, 83);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 33);
            this.btnOK.TabIndex = 8;
            this.btnOK.Text = "确定";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // Strench
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(441, 128);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.txtRight1);
            this.Controls.Add(this.txtLeft1);
            this.Controls.Add(this.lblRight1);
            this.Controls.Add(this.lblLeft1);
            this.Controls.Add(this.txtRight0);
            this.Controls.Add(this.lblRight0);
            this.Controls.Add(this.lblLeft0);
            this.Controls.Add(this.txtLeft0);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Strench";
            this.Text = "任意拉伸";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtLeft0;
        private System.Windows.Forms.Label lblLeft0;
        private System.Windows.Forms.Label lblRight0;
        private System.Windows.Forms.TextBox txtRight0;
        private System.Windows.Forms.Label lblLeft1;
        private System.Windows.Forms.Label lblRight1;
        private System.Windows.Forms.TextBox txtLeft1;
        private System.Windows.Forms.TextBox txtRight1;
        private System.Windows.Forms.Button btnOK;
    }
}