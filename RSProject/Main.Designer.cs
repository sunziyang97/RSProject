﻿namespace RSProject
{
    partial class RSMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RSMain));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileIn = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHeadFile = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.menuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.menuTran = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBSQ = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBIL = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBIP = new System.Windows.Forms.ToolStripMenuItem();
            this.menuATool = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStatistics = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHistogram = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHHistogram = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCumuHistogram = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.menuJointHistogram = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.menuMean = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStandardDeviation = new System.Windows.Forms.ToolStripMenuItem();
            this.menuExtreme = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.menuMeanSingle = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStandardSingle = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCovariance = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCorrelationCoefficient = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.menuSymbioticMatrix = new System.Windows.Forms.ToolStripMenuItem();
            this.menuContrast = new System.Windows.Forms.ToolStripMenuItem();
            this.menuDisplayStretch = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStretch = new System.Windows.Forms.ToolStripMenuItem();
            this.menuLinearStretch = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.menuEqualization = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSpecification = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.menuOIF = new System.Windows.Forms.ToolStripMenuItem();
            this.menuTransform = new System.Windows.Forms.ToolStripMenuItem();
            this.menuKT = new System.Windows.Forms.ToolStripMenuItem();
            this.menuKL = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.menuMath = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.menuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlFileIn = new System.Windows.Forms.Panel();
            this.btnHeadFile = new System.Windows.Forms.Button();
            this.btnFile = new System.Windows.Forms.Button();
            this.lblHeadFile = new System.Windows.Forms.Label();
            this.txtHeadFile = new System.Windows.Forms.TextBox();
            this.txtFile = new System.Windows.Forms.TextBox();
            this.lblFile = new System.Windows.Forms.Label();
            this.pnlInfo = new System.Windows.Forms.Panel();
            this.btnModify = new System.Windows.Forms.Button();
            this.lblInfo = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnView = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.rtb = new System.Windows.Forms.RichTextBox();
            this.menuStrip1.SuspendLayout();
            this.pnlFileIn.SuspendLayout();
            this.pnlInfo.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFile,
            this.menuTran,
            this.menuATool,
            this.menuHelp});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(855, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuFile
            // 
            this.menuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuOpen,
            this.toolStripSeparator1,
            this.menuExit});
            this.menuFile.Name = "menuFile";
            this.menuFile.Size = new System.Drawing.Size(69, 24);
            this.menuFile.Text = "文件(&F)";
            // 
            // menuOpen
            // 
            this.menuOpen.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFileIn,
            this.menuHeadFile});
            this.menuOpen.Name = "menuOpen";
            this.menuOpen.Size = new System.Drawing.Size(136, 26);
            this.menuOpen.Text = "打开(&O)";
            this.menuOpen.Click += new System.EventHandler(this.menuOpen_Click);
            // 
            // menuFileIn
            // 
            this.menuFileIn.Name = "menuFileIn";
            this.menuFileIn.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.menuFileIn.ShowShortcutKeys = false;
            this.menuFileIn.Size = new System.Drawing.Size(151, 26);
            this.menuFileIn.Text = "文件(&F)";
            this.menuFileIn.Click += new System.EventHandler(this.menuFileIn_Click);
            // 
            // menuHeadFile
            // 
            this.menuHeadFile.Name = "menuHeadFile";
            this.menuHeadFile.Size = new System.Drawing.Size(151, 26);
            this.menuHeadFile.Text = "头文件(&H)";
            this.menuHeadFile.Click += new System.EventHandler(this.menuHeadFile_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(133, 6);
            // 
            // menuExit
            // 
            this.menuExit.Name = "menuExit";
            this.menuExit.Size = new System.Drawing.Size(136, 26);
            this.menuExit.Text = "退出(&X)";
            this.menuExit.Click += new System.EventHandler(this.menuExit_Click);
            // 
            // menuTran
            // 
            this.menuTran.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuBSQ,
            this.menuBIL,
            this.menuBIP});
            this.menuTran.Name = "menuTran";
            this.menuTran.Size = new System.Drawing.Size(100, 24);
            this.menuTran.Text = "转换工具(&T)";
            // 
            // menuBSQ
            // 
            this.menuBSQ.Name = "menuBSQ";
            this.menuBSQ.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.menuBSQ.ShowShortcutKeys = false;
            this.menuBSQ.Size = new System.Drawing.Size(127, 26);
            this.menuBSQ.Text = "BSQ(&Q)";
            this.menuBSQ.Click += new System.EventHandler(this.menuBSQ_Click);
            // 
            // menuBIL
            // 
            this.menuBIL.Name = "menuBIL";
            this.menuBIL.Size = new System.Drawing.Size(127, 26);
            this.menuBIL.Text = "BIL(&L)";
            this.menuBIL.Click += new System.EventHandler(this.menuBIL_Click);
            // 
            // menuBIP
            // 
            this.menuBIP.Name = "menuBIP";
            this.menuBIP.Size = new System.Drawing.Size(127, 26);
            this.menuBIP.Text = "BIP(&P)";
            this.menuBIP.Click += new System.EventHandler(this.menuBIP_Click);
            // 
            // menuATool
            // 
            this.menuATool.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuStatistics,
            this.menuDisplayStretch,
            this.menuTransform});
            this.menuATool.Name = "menuATool";
            this.menuATool.Size = new System.Drawing.Size(103, 24);
            this.menuATool.Text = "分析工具(&N)";
            // 
            // menuStatistics
            // 
            this.menuStatistics.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuHistogram,
            this.toolStripSeparator3,
            this.menuMean,
            this.menuStandardDeviation,
            this.menuExtreme,
            this.toolStripSeparator2,
            this.menuMeanSingle,
            this.menuStandardSingle,
            this.menuCovariance,
            this.menuCorrelationCoefficient,
            this.toolStripSeparator4,
            this.menuSymbioticMatrix,
            this.menuContrast});
            this.menuStatistics.Name = "menuStatistics";
            this.menuStatistics.Size = new System.Drawing.Size(181, 26);
            this.menuStatistics.Text = "统计(&S)";
            // 
            // menuHistogram
            // 
            this.menuHistogram.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuHHistogram,
            this.menuCumuHistogram,
            this.toolStripSeparator5,
            this.menuJointHistogram});
            this.menuHistogram.Name = "menuHistogram";
            this.menuHistogram.Size = new System.Drawing.Size(159, 26);
            this.menuHistogram.Text = "直方图";
            // 
            // menuHHistogram
            // 
            this.menuHHistogram.Name = "menuHHistogram";
            this.menuHHistogram.Size = new System.Drawing.Size(159, 26);
            this.menuHHistogram.Text = "直方图";
            this.menuHHistogram.Click += new System.EventHandler(this.menuHHistogram_Click);
            // 
            // menuCumuHistogram
            // 
            this.menuCumuHistogram.Name = "menuCumuHistogram";
            this.menuCumuHistogram.Size = new System.Drawing.Size(159, 26);
            this.menuCumuHistogram.Text = "累计直方图";
            this.menuCumuHistogram.Click += new System.EventHandler(this.menuCumuHistogram_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(156, 6);
            // 
            // menuJointHistogram
            // 
            this.menuJointHistogram.Name = "menuJointHistogram";
            this.menuJointHistogram.Size = new System.Drawing.Size(159, 26);
            this.menuJointHistogram.Text = "联合直方图";
            this.menuJointHistogram.Click += new System.EventHandler(this.menuJointHistogram_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(156, 6);
            // 
            // menuMean
            // 
            this.menuMean.Name = "menuMean";
            this.menuMean.Size = new System.Drawing.Size(159, 26);
            this.menuMean.Text = "总体均值";
            this.menuMean.Click += new System.EventHandler(this.menuMean_Click);
            // 
            // menuStandardDeviation
            // 
            this.menuStandardDeviation.Name = "menuStandardDeviation";
            this.menuStandardDeviation.Size = new System.Drawing.Size(159, 26);
            this.menuStandardDeviation.Text = "总体标准差";
            this.menuStandardDeviation.Click += new System.EventHandler(this.menuStandardDeviation_Click);
            // 
            // menuExtreme
            // 
            this.menuExtreme.Name = "menuExtreme";
            this.menuExtreme.Size = new System.Drawing.Size(159, 26);
            this.menuExtreme.Text = "极差";
            this.menuExtreme.Click += new System.EventHandler(this.menuExtreme_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(156, 6);
            // 
            // menuMeanSingle
            // 
            this.menuMeanSingle.Name = "menuMeanSingle";
            this.menuMeanSingle.Size = new System.Drawing.Size(159, 26);
            this.menuMeanSingle.Text = "均值";
            this.menuMeanSingle.Click += new System.EventHandler(this.menuMeanSingle_Click);
            // 
            // menuStandardSingle
            // 
            this.menuStandardSingle.Name = "menuStandardSingle";
            this.menuStandardSingle.Size = new System.Drawing.Size(159, 26);
            this.menuStandardSingle.Text = "标准差";
            this.menuStandardSingle.Click += new System.EventHandler(this.menuStandardSingle_Click);
            // 
            // menuCovariance
            // 
            this.menuCovariance.Name = "menuCovariance";
            this.menuCovariance.Size = new System.Drawing.Size(159, 26);
            this.menuCovariance.Text = "协方差";
            this.menuCovariance.Click += new System.EventHandler(this.menuCovariance_Click);
            // 
            // menuCorrelationCoefficient
            // 
            this.menuCorrelationCoefficient.Name = "menuCorrelationCoefficient";
            this.menuCorrelationCoefficient.Size = new System.Drawing.Size(159, 26);
            this.menuCorrelationCoefficient.Text = "相关系数";
            this.menuCorrelationCoefficient.Click += new System.EventHandler(this.menuCorrelationCoefficient_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(156, 6);
            // 
            // menuSymbioticMatrix
            // 
            this.menuSymbioticMatrix.Name = "menuSymbioticMatrix";
            this.menuSymbioticMatrix.Size = new System.Drawing.Size(159, 26);
            this.menuSymbioticMatrix.Text = "共生矩阵";
            this.menuSymbioticMatrix.Click += new System.EventHandler(this.menuSymbioticMatrix_Click);
            // 
            // menuContrast
            // 
            this.menuContrast.Name = "menuContrast";
            this.menuContrast.Size = new System.Drawing.Size(159, 26);
            this.menuContrast.Text = "对比度";
            this.menuContrast.Click += new System.EventHandler(this.menuContrast_Click);
            // 
            // menuDisplayStretch
            // 
            this.menuDisplayStretch.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuStretch,
            this.menuLinearStretch,
            this.toolStripSeparator6,
            this.menuEqualization,
            this.menuSpecification,
            this.toolStripSeparator7,
            this.menuOIF});
            this.menuDisplayStretch.Name = "menuDisplayStretch";
            this.menuDisplayStretch.Size = new System.Drawing.Size(181, 26);
            this.menuDisplayStretch.Text = "显示和拉伸(&V)";
            // 
            // menuStretch
            // 
            this.menuStretch.Name = "menuStretch";
            this.menuStretch.Size = new System.Drawing.Size(174, 26);
            this.menuStretch.Text = "任意拉伸";
            this.menuStretch.Click += new System.EventHandler(this.menuStretch_Click);
            // 
            // menuLinearStretch
            // 
            this.menuLinearStretch.Name = "menuLinearStretch";
            this.menuLinearStretch.Size = new System.Drawing.Size(174, 26);
            this.menuLinearStretch.Text = "2%线性拉伸";
            this.menuLinearStretch.Click += new System.EventHandler(this.menuLinearStretch_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(171, 6);
            // 
            // menuEqualization
            // 
            this.menuEqualization.Name = "menuEqualization";
            this.menuEqualization.Size = new System.Drawing.Size(174, 26);
            this.menuEqualization.Text = "直方图均衡化";
            this.menuEqualization.Click += new System.EventHandler(this.menuEqualization_Click);
            // 
            // menuSpecification
            // 
            this.menuSpecification.Name = "menuSpecification";
            this.menuSpecification.Size = new System.Drawing.Size(174, 26);
            this.menuSpecification.Text = "直方图规定化";
            this.menuSpecification.Click += new System.EventHandler(this.menuSpecification_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(171, 6);
            // 
            // menuOIF
            // 
            this.menuOIF.Name = "menuOIF";
            this.menuOIF.Size = new System.Drawing.Size(174, 26);
            this.menuOIF.Text = "OIF值计算";
            this.menuOIF.Click += new System.EventHandler(this.menuOIF_Click);
            // 
            // menuTransform
            // 
            this.menuTransform.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuKT,
            this.menuKL,
            this.toolStripSeparator8,
            this.menuMath});
            this.menuTransform.Name = "menuTransform";
            this.menuTransform.Size = new System.Drawing.Size(181, 26);
            this.menuTransform.Text = "图像变换(&T)";
            // 
            // menuKT
            // 
            this.menuKT.Name = "menuKT";
            this.menuKT.Size = new System.Drawing.Size(181, 26);
            this.menuKT.Text = "K-T变换";
            // 
            // menuKL
            // 
            this.menuKL.Name = "menuKL";
            this.menuKL.Size = new System.Drawing.Size(181, 26);
            this.menuKL.Text = "K-L变换";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(178, 6);
            // 
            // menuMath
            // 
            this.menuMath.Name = "menuMath";
            this.menuMath.Size = new System.Drawing.Size(181, 26);
            this.menuMath.Text = "波段计算";
            // 
            // menuHelp
            // 
            this.menuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuAbout});
            this.menuHelp.Name = "menuHelp";
            this.menuHelp.Size = new System.Drawing.Size(73, 24);
            this.menuHelp.Text = "帮助(&H)";
            // 
            // menuAbout
            // 
            this.menuAbout.Name = "menuAbout";
            this.menuAbout.Size = new System.Drawing.Size(165, 26);
            this.menuAbout.Text = "关于我们(&A)";
            this.menuAbout.Click += new System.EventHandler(this.menuAbout_Click);
            // 
            // pnlFileIn
            // 
            this.pnlFileIn.Controls.Add(this.btnHeadFile);
            this.pnlFileIn.Controls.Add(this.btnFile);
            this.pnlFileIn.Controls.Add(this.lblHeadFile);
            this.pnlFileIn.Controls.Add(this.txtHeadFile);
            this.pnlFileIn.Controls.Add(this.txtFile);
            this.pnlFileIn.Controls.Add(this.lblFile);
            this.pnlFileIn.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlFileIn.Location = new System.Drawing.Point(0, 28);
            this.pnlFileIn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlFileIn.Name = "pnlFileIn";
            this.pnlFileIn.Size = new System.Drawing.Size(855, 108);
            this.pnlFileIn.TabIndex = 1;
            // 
            // btnHeadFile
            // 
            this.btnHeadFile.Location = new System.Drawing.Point(768, 61);
            this.btnHeadFile.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnHeadFile.Name = "btnHeadFile";
            this.btnHeadFile.Size = new System.Drawing.Size(75, 25);
            this.btnHeadFile.TabIndex = 5;
            this.btnHeadFile.Text = "打开";
            this.btnHeadFile.UseVisualStyleBackColor = true;
            this.btnHeadFile.Click += new System.EventHandler(this.btnHeadFile_Click);
            // 
            // btnFile
            // 
            this.btnFile.Location = new System.Drawing.Point(768, 15);
            this.btnFile.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnFile.Name = "btnFile";
            this.btnFile.Size = new System.Drawing.Size(75, 25);
            this.btnFile.TabIndex = 4;
            this.btnFile.Text = "打开";
            this.btnFile.UseVisualStyleBackColor = true;
            this.btnFile.Click += new System.EventHandler(this.btnFile_Click);
            // 
            // lblHeadFile
            // 
            this.lblHeadFile.AutoSize = true;
            this.lblHeadFile.Location = new System.Drawing.Point(29, 64);
            this.lblHeadFile.Name = "lblHeadFile";
            this.lblHeadFile.Size = new System.Drawing.Size(52, 15);
            this.lblHeadFile.TabIndex = 3;
            this.lblHeadFile.Text = "头文件";
            // 
            // txtHeadFile
            // 
            this.txtHeadFile.Location = new System.Drawing.Point(104, 61);
            this.txtHeadFile.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtHeadFile.Name = "txtHeadFile";
            this.txtHeadFile.ReadOnly = true;
            this.txtHeadFile.Size = new System.Drawing.Size(642, 25);
            this.txtHeadFile.TabIndex = 2;
            this.txtHeadFile.TextChanged += new System.EventHandler(this.txtHeadFile_TextChanged);
            // 
            // txtFile
            // 
            this.txtFile.Location = new System.Drawing.Point(104, 15);
            this.txtFile.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtFile.Name = "txtFile";
            this.txtFile.ReadOnly = true;
            this.txtFile.Size = new System.Drawing.Size(642, 25);
            this.txtFile.TabIndex = 1;
            // 
            // lblFile
            // 
            this.lblFile.AutoSize = true;
            this.lblFile.Location = new System.Drawing.Point(29, 18);
            this.lblFile.Name = "lblFile";
            this.lblFile.Size = new System.Drawing.Size(37, 15);
            this.lblFile.TabIndex = 0;
            this.lblFile.Text = "文件";
            // 
            // pnlInfo
            // 
            this.pnlInfo.Controls.Add(this.btnModify);
            this.pnlInfo.Controls.Add(this.lblInfo);
            this.pnlInfo.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlInfo.Location = new System.Drawing.Point(0, 136);
            this.pnlInfo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlInfo.Name = "pnlInfo";
            this.pnlInfo.Size = new System.Drawing.Size(247, 331);
            this.pnlInfo.TabIndex = 3;
            // 
            // btnModify
            // 
            this.btnModify.Location = new System.Drawing.Point(156, 295);
            this.btnModify.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnModify.Name = "btnModify";
            this.btnModify.Size = new System.Drawing.Size(75, 25);
            this.btnModify.TabIndex = 1;
            this.btnModify.Text = "修改";
            this.btnModify.UseVisualStyleBackColor = true;
            this.btnModify.Click += new System.EventHandler(this.btnModify_Click);
            // 
            // lblInfo
            // 
            this.lblInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblInfo.Location = new System.Drawing.Point(0, 0);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(247, 283);
            this.lblInfo.TabIndex = 0;
            this.lblInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnView);
            this.panel1.Controls.Add(this.btnExport);
            this.panel1.Controls.Add(this.rtb);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(247, 136);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(608, 331);
            this.panel1.TabIndex = 4;
            // 
            // btnView
            // 
            this.btnView.Location = new System.Drawing.Point(390, 295);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(109, 26);
            this.btnView.TabIndex = 3;
            this.btnView.Text = "图像显示";
            this.btnView.UseVisualStyleBackColor = true;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(521, 295);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(75, 25);
            this.btnExport.TabIndex = 1;
            this.btnExport.Text = "导出";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // rtb
            // 
            this.rtb.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtb.Dock = System.Windows.Forms.DockStyle.Top;
            this.rtb.Location = new System.Drawing.Point(0, 0);
            this.rtb.Name = "rtb";
            this.rtb.ReadOnly = true;
            this.rtb.Size = new System.Drawing.Size(608, 283);
            this.rtb.TabIndex = 0;
            this.rtb.Text = "";
            // 
            // RSMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(855, 467);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlInfo);
            this.Controls.Add(this.pnlFileIn);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RSMain";
            this.Text = "RSProject";
            this.Load += new System.EventHandler(this.RSMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.pnlFileIn.ResumeLayout(false);
            this.pnlFileIn.PerformLayout();
            this.pnlInfo.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuFile;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem menuExit;
        private System.Windows.Forms.ToolStripMenuItem menuATool;
        private System.Windows.Forms.ToolStripMenuItem menuStatistics;
        private System.Windows.Forms.ToolStripMenuItem menuHistogram;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem menuMean;
        private System.Windows.Forms.ToolStripMenuItem menuStandardDeviation;
        private System.Windows.Forms.ToolStripMenuItem menuCorrelationCoefficient;
        private System.Windows.Forms.ToolStripMenuItem menuCovariance;
        private System.Windows.Forms.ToolStripMenuItem menuExtreme;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem menuSymbioticMatrix;
        private System.Windows.Forms.Panel pnlFileIn;
        private System.Windows.Forms.Label lblHeadFile;
        private System.Windows.Forms.TextBox txtHeadFile;
        private System.Windows.Forms.TextBox txtFile;
        private System.Windows.Forms.Label lblFile;
        private System.Windows.Forms.Button btnHeadFile;
        private System.Windows.Forms.Button btnFile;
        private System.Windows.Forms.ToolStripMenuItem menuTran;
        private System.Windows.Forms.ToolStripMenuItem menuBIL;
        private System.Windows.Forms.ToolStripMenuItem menuBIP;
        private System.Windows.Forms.ToolStripMenuItem menuBSQ;
        private System.Windows.Forms.ToolStripMenuItem menuHelp;
        private System.Windows.Forms.ToolStripMenuItem menuAbout;
        private System.Windows.Forms.ToolStripMenuItem menuOpen;
        private System.Windows.Forms.ToolStripMenuItem menuFileIn;
        private System.Windows.Forms.Panel pnlInfo;
        private System.Windows.Forms.ToolStripMenuItem menuHeadFile;
        private System.Windows.Forms.Button btnModify;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RichTextBox rtb;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.ToolStripMenuItem menuMeanSingle;
        private System.Windows.Forms.ToolStripMenuItem menuStandardSingle;
        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.ToolStripMenuItem menuHHistogram;
        private System.Windows.Forms.ToolStripMenuItem menuCumuHistogram;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem menuJointHistogram;
        private System.Windows.Forms.ToolStripMenuItem menuContrast;
        private System.Windows.Forms.ToolStripMenuItem menuDisplayStretch;
        private System.Windows.Forms.ToolStripMenuItem menuLinearStretch;
        private System.Windows.Forms.ToolStripMenuItem menuStretch;
        private System.Windows.Forms.ToolStripMenuItem menuOIF;
        private System.Windows.Forms.ToolStripMenuItem menuEqualization;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem menuSpecification;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem menuTransform;
        private System.Windows.Forms.ToolStripMenuItem menuKT;
        private System.Windows.Forms.ToolStripMenuItem menuKL;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem menuMath;
    }
}

