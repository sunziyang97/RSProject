﻿namespace RSProject
{
    partial class SymbioticMatrix
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gpbSymbioticMatrix = new System.Windows.Forms.GroupBox();
            this.lblA = new System.Windows.Forms.Label();
            this.lblB = new System.Windows.Forms.Label();
            this.txtA = new System.Windows.Forms.TextBox();
            this.txtB = new System.Windows.Forms.TextBox();
            this.lblOK = new System.Windows.Forms.Button();
            this.gpbSymbioticMatrix.SuspendLayout();
            this.SuspendLayout();
            // 
            // gpbSymbioticMatrix
            // 
            this.gpbSymbioticMatrix.Controls.Add(this.lblOK);
            this.gpbSymbioticMatrix.Controls.Add(this.txtB);
            this.gpbSymbioticMatrix.Controls.Add(this.txtA);
            this.gpbSymbioticMatrix.Controls.Add(this.lblB);
            this.gpbSymbioticMatrix.Controls.Add(this.lblA);
            this.gpbSymbioticMatrix.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gpbSymbioticMatrix.Location = new System.Drawing.Point(0, 0);
            this.gpbSymbioticMatrix.Name = "gpbSymbioticMatrix";
            this.gpbSymbioticMatrix.Size = new System.Drawing.Size(158, 146);
            this.gpbSymbioticMatrix.TabIndex = 0;
            this.gpbSymbioticMatrix.TabStop = false;
            this.gpbSymbioticMatrix.Text = "距离(a,b)设定";
            // 
            // lblA
            // 
            this.lblA.AutoSize = true;
            this.lblA.Location = new System.Drawing.Point(23, 31);
            this.lblA.Name = "lblA";
            this.lblA.Size = new System.Drawing.Size(24, 17);
            this.lblA.TabIndex = 0;
            this.lblA.Text = "a :";
            // 
            // lblB
            // 
            this.lblB.AutoSize = true;
            this.lblB.Location = new System.Drawing.Point(23, 65);
            this.lblB.Name = "lblB";
            this.lblB.Size = new System.Drawing.Size(24, 17);
            this.lblB.TabIndex = 1;
            this.lblB.Text = "b :";
            // 
            // txtA
            // 
            this.txtA.Location = new System.Drawing.Point(53, 31);
            this.txtA.Name = "txtA";
            this.txtA.Size = new System.Drawing.Size(76, 22);
            this.txtA.TabIndex = 2;
            // 
            // txtB
            // 
            this.txtB.Location = new System.Drawing.Point(53, 62);
            this.txtB.Name = "txtB";
            this.txtB.Size = new System.Drawing.Size(76, 22);
            this.txtB.TabIndex = 3;
            // 
            // lblOK
            // 
            this.lblOK.Location = new System.Drawing.Point(53, 102);
            this.lblOK.Name = "lblOK";
            this.lblOK.Size = new System.Drawing.Size(75, 32);
            this.lblOK.TabIndex = 4;
            this.lblOK.Text = "确定";
            this.lblOK.UseVisualStyleBackColor = true;
            this.lblOK.Click += new System.EventHandler(this.lblOK_Click);
            // 
            // SymbioticMatrix
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(158, 146);
            this.Controls.Add(this.gpbSymbioticMatrix);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SymbioticMatrix";
            this.Text = "共生矩阵参数";
            this.gpbSymbioticMatrix.ResumeLayout(false);
            this.gpbSymbioticMatrix.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gpbSymbioticMatrix;
        private System.Windows.Forms.Label lblB;
        private System.Windows.Forms.Label lblA;
        private System.Windows.Forms.Button lblOK;
        private System.Windows.Forms.TextBox txtB;
        private System.Windows.Forms.TextBox txtA;
    }
}