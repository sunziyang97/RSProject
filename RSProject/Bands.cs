﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RSProject
{
    public partial class Bands : Form
    {
        public int band1, band2, band3, flag;

        public Bands(int band,int flag)
        {
            StartPosition = FormStartPosition.CenterParent; ;
            InitializeComponent();
            this.flag = flag;
            if (flag == 1)
            {
                lblBand2.Enabled = false;
                lblBand2.Visible = false;
                lblBand3.Enabled = false;
                lblBand3.Visible = false;
                cbxBand2.Enabled = false;
                cbxBand2.Visible = false;
                cbxBand3.Enabled = false;
                cbxBand3.Visible = false;
                btnOK.Location = new Point(165, 59);
                Size = new Size(273, 151);
                lblBand1.Text = "选择波段：";
                for (int i = 1; i <= band; i++)
                {
                    cbxBand1.Items.Add(i);
                }
            }
            if(flag==2)
            {
                lblBand3.Enabled = false;
                lblBand3.Visible = false;
                cbxBand3.Enabled = false;
                cbxBand3.Visible = false;
                btnOK.Location = new Point(165, 104);
                Size = new Size(273, 196);
                for (int i = 1; i <= band; i++)
                {
                    cbxBand1.Items.Add(i);
                    cbxBand2.Items.Add(i);
                }
            }
            if(flag==3)
            {
                for (int i = 1; i <= band; i++)
                {
                    lblBand1.Text = "RED：";
                    lblBand2.Text = "GREEN：";
                    lblBand3.Text = "BLUE：";
                    cbxBand1.Items.Add(i);
                    cbxBand2.Items.Add(i);
                    cbxBand3.Items.Add(i);
                }
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            band1 = int.Parse(cbxBand1.Text);
            if (flag == 2)
                band2 = int.Parse(cbxBand2.Text);
            if (flag == 3)
            {
                band2 = int.Parse(cbxBand2.Text);
                band3 = int.Parse(cbxBand3.Text);
            }
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
