﻿namespace RSProject
{
    partial class Info
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSample = new System.Windows.Forms.Label();
            this.txtSample = new System.Windows.Forms.TextBox();
            this.lblLine = new System.Windows.Forms.Label();
            this.txtLine = new System.Windows.Forms.TextBox();
            this.lblBand = new System.Windows.Forms.Label();
            this.txtBand = new System.Windows.Forms.TextBox();
            this.lblDataType = new System.Windows.Forms.Label();
            this.txtDataType = new System.Windows.Forms.TextBox();
            this.lblInter = new System.Windows.Forms.Label();
            this.txtInter = new System.Windows.Forms.TextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.lblOffset = new System.Windows.Forms.Label();
            this.txtOffset = new System.Windows.Forms.TextBox();
            this.lblFileType = new System.Windows.Forms.Label();
            this.txtFileType = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblSample
            // 
            this.lblSample.AutoSize = true;
            this.lblSample.Location = new System.Drawing.Point(13, 16);
            this.lblSample.Name = "lblSample";
            this.lblSample.Size = new System.Drawing.Size(63, 15);
            this.lblSample.TabIndex = 0;
            this.lblSample.Text = "Samples";
            // 
            // txtSample
            // 
            this.txtSample.Location = new System.Drawing.Point(89, 13);
            this.txtSample.Name = "txtSample";
            this.txtSample.Size = new System.Drawing.Size(63, 25);
            this.txtSample.TabIndex = 1;
            // 
            // lblLine
            // 
            this.lblLine.AutoSize = true;
            this.lblLine.Location = new System.Drawing.Point(13, 63);
            this.lblLine.Name = "lblLine";
            this.lblLine.Size = new System.Drawing.Size(47, 15);
            this.lblLine.TabIndex = 2;
            this.lblLine.Text = "Lines";
            // 
            // txtLine
            // 
            this.txtLine.Location = new System.Drawing.Point(89, 60);
            this.txtLine.Name = "txtLine";
            this.txtLine.Size = new System.Drawing.Size(63, 25);
            this.txtLine.TabIndex = 3;
            // 
            // lblBand
            // 
            this.lblBand.AutoSize = true;
            this.lblBand.Location = new System.Drawing.Point(13, 117);
            this.lblBand.Name = "lblBand";
            this.lblBand.Size = new System.Drawing.Size(47, 15);
            this.lblBand.TabIndex = 4;
            this.lblBand.Text = "Bands";
            // 
            // txtBand
            // 
            this.txtBand.Location = new System.Drawing.Point(89, 114);
            this.txtBand.Name = "txtBand";
            this.txtBand.Size = new System.Drawing.Size(63, 25);
            this.txtBand.TabIndex = 5;
            // 
            // lblDataType
            // 
            this.lblDataType.AutoSize = true;
            this.lblDataType.Location = new System.Drawing.Point(202, 63);
            this.lblDataType.Name = "lblDataType";
            this.lblDataType.Size = new System.Drawing.Size(79, 15);
            this.lblDataType.TabIndex = 6;
            this.lblDataType.Text = "Data Type";
            // 
            // txtDataType
            // 
            this.txtDataType.Location = new System.Drawing.Point(298, 60);
            this.txtDataType.Name = "txtDataType";
            this.txtDataType.Size = new System.Drawing.Size(131, 25);
            this.txtDataType.TabIndex = 7;
            // 
            // lblInter
            // 
            this.lblInter.AutoSize = true;
            this.lblInter.Location = new System.Drawing.Point(202, 117);
            this.lblInter.Name = "lblInter";
            this.lblInter.Size = new System.Drawing.Size(87, 15);
            this.lblInter.TabIndex = 8;
            this.lblInter.Text = "Interleave";
            // 
            // txtInter
            // 
            this.txtInter.Location = new System.Drawing.Point(298, 114);
            this.txtInter.Name = "txtInter";
            this.txtInter.Size = new System.Drawing.Size(131, 25);
            this.txtInter.TabIndex = 9;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(354, 164);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 10;
            this.btnOK.Text = "确定";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblOffset
            // 
            this.lblOffset.AutoSize = true;
            this.lblOffset.Location = new System.Drawing.Point(16, 165);
            this.lblOffset.Name = "lblOffset";
            this.lblOffset.Size = new System.Drawing.Size(55, 15);
            this.lblOffset.TabIndex = 11;
            this.lblOffset.Text = "Offset";
            // 
            // txtOffset
            // 
            this.txtOffset.Location = new System.Drawing.Point(89, 162);
            this.txtOffset.Name = "txtOffset";
            this.txtOffset.Size = new System.Drawing.Size(63, 25);
            this.txtOffset.TabIndex = 12;
            // 
            // lblFileType
            // 
            this.lblFileType.AutoSize = true;
            this.lblFileType.Location = new System.Drawing.Point(205, 16);
            this.lblFileType.Name = "lblFileType";
            this.lblFileType.Size = new System.Drawing.Size(79, 15);
            this.lblFileType.TabIndex = 13;
            this.lblFileType.Text = "File Type";
            // 
            // txtFileType
            // 
            this.txtFileType.Location = new System.Drawing.Point(298, 12);
            this.txtFileType.Name = "txtFileType";
            this.txtFileType.Size = new System.Drawing.Size(131, 25);
            this.txtFileType.TabIndex = 14;
            // 
            // Info
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(447, 205);
            this.Controls.Add(this.txtFileType);
            this.Controls.Add(this.lblFileType);
            this.Controls.Add(this.txtOffset);
            this.Controls.Add(this.lblOffset);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.txtInter);
            this.Controls.Add(this.lblInter);
            this.Controls.Add(this.txtDataType);
            this.Controls.Add(this.lblDataType);
            this.Controls.Add(this.txtBand);
            this.Controls.Add(this.lblBand);
            this.Controls.Add(this.txtLine);
            this.Controls.Add(this.lblLine);
            this.Controls.Add(this.txtSample);
            this.Controls.Add(this.lblSample);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Info";
            this.Text = "修改文件信息";
            this.Load += new System.EventHandler(this.Info_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSample;
        private System.Windows.Forms.TextBox txtSample;
        private System.Windows.Forms.Label lblLine;
        private System.Windows.Forms.TextBox txtLine;
        private System.Windows.Forms.Label lblBand;
        private System.Windows.Forms.TextBox txtBand;
        private System.Windows.Forms.Label lblDataType;
        private System.Windows.Forms.TextBox txtDataType;
        private System.Windows.Forms.Label lblInter;
        private System.Windows.Forms.TextBox txtInter;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label lblOffset;
        private System.Windows.Forms.TextBox txtOffset;
        private System.Windows.Forms.Label lblFileType;
        private System.Windows.Forms.TextBox txtFileType;
    }
}