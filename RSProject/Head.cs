﻿namespace RSProject
{
    /// <summary>
    /// 头文件
    /// </summary>
    public class Head
    {
        public int samples;
        public int lines;
        public int bands;
        public int header_offset;
        public string file_type;
        public int data_type;
        public string interleave;

    }
}
