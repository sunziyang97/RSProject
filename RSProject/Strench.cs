﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RSProject
{
    public partial class Strench : Form
    {
        public int left0, left1, right0, right1;
        public Strench()
        {
            StartPosition = FormStartPosition.CenterParent; ;
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            left0 = int.Parse(txtLeft0.Text);
            left1 = int.Parse(txtLeft1.Text);
            right0 = int.Parse(txtRight0.Text);
            right1 = int.Parse(txtRight1.Text);
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
