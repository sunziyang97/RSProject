﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace RSProject
{
    public partial class RSMain : Form
    {
        public RSMain()
        {
            StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
        }

        private void txtHeadFile_TextChanged(object sender, EventArgs e)
        {
            if (txtHeadFile.Text != "")
            {
                Global.data.read_metadata(txtHeadFile.Text, ref Global.head);
                if (Global.dataPath.Length > 1)
                {
                    Global.head.bands = Global.dataPath.Length;
                    Global.data.mergedata(Global.dataPath, ref Global.intArray, ref Global.dataArray, ref Global.head);
                }
                else
                {
                    Global.data.read_data(Global.dataPath[0], ref Global.intArray, ref Global.dataArray, ref Global.head);
                }
                lblInfo.Text = "文件信息:\nSamples: " + Global.head.samples;
                lblInfo.Text += "\n\nLines: " + Global.head.lines;
                lblInfo.Text += "\n\nBands: " + Global.head.bands;
                lblInfo.Text += "\n\nOffset: " + Global.head.header_offset;
                lblInfo.Text += "\n\nFile Type: " + Global.head.file_type;
                lblInfo.Text += "\n\nData Type: " + Global.head.data_type;
                lblInfo.Text += "\n\nInterleave: " + Global.head.interleave;
            }
        }

        private void btnHeadFile_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog OpenFD = new OpenFileDialog())     //实例化一个 OpenFileDialog 的对象
            {
                OpenFD.Filter = "head File (*.hdr)|*.hdr|" + "All files (*.*)|*.*";
                //定义打开的默认文件夹位置
                OpenFD.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                if (OpenFD.ShowDialog() == DialogResult.OK)                            //显示打开本地文件的窗体
                {
                    txtHeadFile.Text = OpenFD.FileName;                 //将 路径名称 显示在 textBox 控件上txtH
                }
            }
        }

        private void menuFileIn_Click(object sender, EventArgs e)
        {
            btnFile_Click(sender, e);
        }

        private void menuBSQ_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                Global.data.ToBSQ(sfd.FileName,Global.head,Global.dataArray);
            }
        }

        private void menuOpen_Click(object sender, EventArgs e)
        {

        }

        private void menuHeadFile_Click(object sender, EventArgs e)
        {
            btnHeadFile_Click(sender, e);
        }

        private void menuBIL_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                Global.data.ToBIL(sfd.FileName,Global.head,Global.dataArray);
            }
        }

        private void menuBIP_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                Global.data.ToBIP(sfd.FileName,Global.head,Global.dataArray);
            }
        }

        private void menuAbout_Click(object sender, EventArgs e)
        {
            MessageBox.Show("小组成员：\n  董婧雯  孙梓洋  张劲松\n  陆佳莺  金榕榕  唐以洁\n  江游  夏雨晴  汪倩如\n  温旻  邓浩坤", "关于我们");
        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            Info info = new Info();
            info.ShowDialog();
        }

        private void RSMain_Load(object sender, EventArgs e)
        {
            lblInfo.Text = "文件信息:\nSamples: " + Global.head.samples;
            lblInfo.Text += "\n\nLines: " + Global.head.lines;
            lblInfo.Text += "\n\nBands: " + Global.head.bands;
            lblInfo.Text += "\n\nOffset: " + Global.head.header_offset;
            lblInfo.Text += "\n\nFile Type: " + Global.head.file_type;
            lblInfo.Text += "\n\nData Type: " + Global.head.data_type;
            lblInfo.Text += "\n\nInterleave: " + Global.head.interleave;
        }

        private void menuMean_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            funcStatistics funcS = new funcStatistics();
            sfd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                funcS.Mean(sfd.FileName);
            }
        }

        private void menuStandardDeviation_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            funcStatistics funcS = new funcStatistics();
            sfd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                funcS.StandardDeviation(sfd.FileName);
            }
        }

        private void menuCovariance_Click(object sender, EventArgs e)
        {
            funcStatistics funcS = new funcStatistics();
            double[,] Cov = funcS.Covariance();
            string[,] temp = new string[Global.head.bands + 1, Global.head.bands + 1];
            temp[0, 0] = "bands";
            for (int i = 0; i < Global.head.bands; i++)
            {
                temp[0, i + 1] = "band" + i.ToString();
            }

            for (int i = 0; i < Global.head.bands; i++)
            {
                temp[i + 1, 0] = "band" + i.ToString();
                for (int j = 0; j < Global.head.bands; j++)
                {
                    temp[i + 1, j + 1] = Cov[i, j].ToString("F2");
                }
            }

            rtb.Text = "";
            for (int i = 0; i <= Global.head.bands; i++)
            {
                for (int j = 0; j <= Global.head.bands; j++)
                {
                    rtb.Text += temp[i, j] + "\t";
                }
                rtb.Text += "\n";
            }

        }

        private void menuCorrelationCoefficient_Click(object sender, EventArgs e)
        {
            funcStatistics funcS = new funcStatistics();
            double[,] Cor = funcS.CorrelationCoefficient();
            string[,] temp = new string[Global.head.bands + 1, Global.head.bands + 1];
            temp[0, 0] = "bands";
            for (int i = 0; i < Global.head.bands; i++)
            {
                temp[0, i + 1] = "band" + i.ToString();
            }

            for (int i = 0; i < Global.head.bands; i++)
            {
                temp[i + 1, 0] = "band" + i.ToString();
                for (int j = 0; j < Global.head.bands; j++)
                {
                    temp[i + 1, j + 1] = Cor[i, j].ToString("F2");
                }
            }

            rtb.Text = "";
            for (int i = 0; i <= Global.head.bands; i++)
            {
                for (int j = 0; j <= Global.head.bands; j++)
                {
                    rtb.Text += temp[i, j] + "\t";
                }
                rtb.Text += "\n";
            }
        }

        private void menuExtreme_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            funcStatistics funcS = new funcStatistics();
            sfd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                funcS.Extreme(sfd.FileName);
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "文本文档 (*.txt)|*.txt|" + "All files (*.*)|*.*";
            sfd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                rtb.SaveFile(sfd.FileName, RichTextBoxStreamType.PlainText);
                MessageBox.Show("成功！");
            }
        }

        private void menuStandardSingle_Click(object sender, EventArgs e)
        {
            funcStatistics funcS = new funcStatistics();
            double[] standard = funcS.StandardSingle();
            string[,] temp = new string[Global.head.bands + 1, 2];
            temp[0, 0] = "bands";
            temp[0, 1] = "StandardDeviation";

            for (int i = 0; i < Global.head.bands; i++)
            {
                temp[i + 1, 0] = "band" + i.ToString();
                temp[i + 1, 1] = standard[i].ToString("F2");
            }

            rtb.Text = "";
            for (int i = 0; i < Global.head.bands + 1; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    rtb.Text += temp[i, j] + "\t";
                }
                rtb.Text += "\n";
            }
        }

        private void menuMeanSingle_Click(object sender, EventArgs e)
        {
            funcStatistics funcS = new funcStatistics();
            double[] mean = funcS.MeanSingle();
            string[,] temp = new string[Global.head.bands + 1, 2];
            temp[0, 0] = "bands";
            temp[0, 1] = "Mean";

            for (int i = 0; i < Global.head.bands; i++)
            {
                temp[i + 1, 0] = "band" + i.ToString();
                temp[i + 1, 1] = mean[i].ToString("F2");
            }

            rtb.Text = "";
            for (int i = 0; i < Global.head.bands + 1; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    rtb.Text += temp[i, j] + "\t";
                }
                rtb.Text += "\n";
            }

        }

        private void btnView_Click(object sender, EventArgs e)
        {
            Bands data = new Bands(Global.head.bands, 3);
            int band1, band2, band3;
            if (data.ShowDialog() == DialogResult.OK)
            {
                band1 = data.band1;
                band2 = data.band2;
                band3 = data.band3;
                funcStretch funcSt = new funcStretch();
                funcSt.Equalization(Global.intArray, out int[,,] tempArray,Global.head);
                byte[,,] tempData = new byte[100, 1000, 1000];

                for (int i = 0; i < Global.head.bands; i++)
                {
                    for (int j = 0; j < Global.head.lines; j++)
                    {
                        for (int k = 0; k < Global.head.samples; k++)
                        {
                            tempData[i, j, k] = Convert.ToByte(tempArray[i, j, k]);
                        }
                    }
                }

                dataView view = new dataView(band1, band2, band3, tempData);
                view.ShowDialog();
            }
        }

        private void menuHHistogram_Click(object sender, EventArgs e)
        {
            if (Global.head.bands != 0)
            {
                funcStatistics funcS = new funcStatistics();
                funcS.funcHistogram();
            }
        }

        private void menuCumuHistogram_Click(object sender, EventArgs e)
        {
            if (Global.head.bands != 0)
            {
                funcStatistics funcS = new funcStatistics();
                funcS.CumuHistogram();
            }
        }

        private void menuJointHistogram_Click(object sender, EventArgs e)
        {
            int band1 = 0, band2 = 0;
            if (Global.head.bands != 0)
            {
                funcStatistics funcS = new funcStatistics();
                funcS.JointHistogram(ref band1, ref band2);
            }

            //生成联合直方图
            int[,] b = new int[256, 256];
            int m = band1 - 1;
            int n = band2 - 1;
            rtb.Clear();

            for (int j = 0; j < Global.head.lines; j++)
            {
                for (int k = 0; k < Global.head.samples; k++)
                {
                    b[Global.intArray[m, j, k], Global.intArray[n, j, k]]++;
                }
            }
            for (int p = 0; p < 256; p++)
            {
                for (int q = 0; q < 256; q++)
                {
                    if (b[p, q] != 0)
                    {
                        rtb.AppendText("x:" + p + ",y:" + q + "，个数：" + b[p, q] + "\n");
                    }
                }
            }
        }

        private void menuSymbioticMatrix_Click(object sender, EventArgs e)
        {
            funcStatistics funcS = new funcStatistics();
            funcS.SymbioticMatrix();
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "文本文档 (*.txt)|*.txt|" + "All files (*.*)|*.*";
            sfd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                string outpath = sfd.FileName;
                StreamWriter sw = new StreamWriter(File.Open(outpath, FileMode.Create));
                for (int u = 0; u <= 255; u++)
                {
                    for (int v = 0; v <= 255; v++)
                    {
                        sw.Write(Global.F[u, v]);
                        sw.Write(" ");
                    }
                }

                sw.Close();
                MessageBox.Show("成功！");
            }
        }

        private void menuContrast_Click(object sender, EventArgs e)
        {
            int contrast;
            funcStatistics funcS = new funcStatistics();
            contrast = funcS.Contrast();
            rtb.Text = "对比度：" + contrast;
        }

        private void menuLinearStretch_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();

            funcStretch funcS = new funcStretch();
            sfd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                funcS.twoLinearStretch(sfd.FileName);
            }
        }

        private void menuStretch_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                Strench str = new Strench();
                if (str.DialogResult == DialogResult.OK)
                {
                    funcStretch funcS = new funcStretch();
                    int left0 = str.left0;
                    int right0 = str.right0;
                    int left1 = str.left1;
                    int right1 = str.right1;
                    funcS.LinearStretch(sfd.FileName, left0, right0, left1, right1);
                }
            }
        }

        private void menuOIF_Click(object sender, EventArgs e)
        {
            int band1, band2, band3;
            funcStretch funcS = new funcStretch();
            Bands oif = new Bands(Global.head.bands, 3);
            if (oif.ShowDialog() == DialogResult.OK)
            {
                band1 = oif.band1;
                band2 = oif.band2;
                band3 = oif.band3;
                double OIF = funcS.CalOIF(band1 - 1, band2 - 1, band3 - 1);
                rtb.Text = "OIF值：" + OIF;
            }
        }

        private void menuEqualization_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                funcStretch funcS = new funcStretch();

                // 变换后的灰度级
                byte[,,] tempData = new byte[10, 1000, 1000];

                funcS.Equalization(Global.intArray, out int[,,] tempArray,Global.head);

                string P = sfd.FileName;

                string s_data = P;
                string s_metadata = P + ".hdr";
                Global.data.write_metadata(s_metadata, "bsq",Global.head);

                BinaryWriter bw = new BinaryWriter(File.Open(s_data, FileMode.Create));
                for (int i = 0; i < Global.head.bands; i++)
                {
                    for (int j = 0; j < Global.head.lines; j++)
                    {
                        for (int k = 0; k < Global.head.samples; k++)
                        {
                            int a=tempArray[i, j, k];
                            bw.Write(Convert.ToByte(a));
                        }
                    }
                }
                bw.Close();
                MessageBox.Show("成功！");
            }
        }

        private void menuSpecification_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                funcStretch funcS = new funcStretch();
                funcS.Specification(ofd.FileName);
            }
        }

        private void menuExit_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(0);
        }

        private void btnFile_Click(object sender, EventArgs e)
        {
            string[] fileName;
            string metapath;
            using (OpenFileDialog OpenFD = new OpenFileDialog())     //实例化一个 OpenFileDialog 的对象
            {
                OpenFD.Multiselect = true;
                //定义打开的默认文件夹位置
                OpenFD.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                if (OpenFD.ShowDialog() == DialogResult.OK)                            //显示打开本地文件的窗体
                {
                    Global.dataPath = OpenFD.FileNames;                    //把 完整路径名称 赋给 dataPath
                    fileName = OpenFD.SafeFileNames;                //把 文件名称 赋给 fileName

                    if (fileName.Length == 1)
                        txtFile.Text = Global.dataPath[0];
                    else
                    {
                        txtFile.Text = "";
                        for (int i = 0; i < fileName.Length; i++)       //将 文件名称 显示在 textBox 控件上
                        {
                            txtFile.Text += fileName[i] + "; ";
                        }
                    }
                    for (int i = 0; i < Global.dataPath.Length; i++)
                    {
                        metapath = Global.dataPath[i] + ".hdr";
                        if (File.Exists(metapath))
                        {
                            txtHeadFile.Text = metapath;
                            break;
                        }
                    }
                }
            }
        }
    }
}
