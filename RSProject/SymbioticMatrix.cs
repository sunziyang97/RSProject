﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RSProject
{
    public partial class SymbioticMatrix : Form
    {
        public int a, b;
        public SymbioticMatrix()
        {
            StartPosition = FormStartPosition.CenterParent; ;
            InitializeComponent();
        }

        private void lblOK_Click(object sender, EventArgs e)
        {
            a = int.Parse(txtA.Text);
            b = int.Parse(txtB.Text);
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
