﻿namespace RSProject
{
    partial class Bands
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblBand1 = new System.Windows.Forms.Label();
            this.lblBand2 = new System.Windows.Forms.Label();
            this.cbxBand1 = new System.Windows.Forms.ComboBox();
            this.cbxBand2 = new System.Windows.Forms.ComboBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.lblBand3 = new System.Windows.Forms.Label();
            this.cbxBand3 = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lblBand1
            // 
            this.lblBand1.AutoSize = true;
            this.lblBand1.Location = new System.Drawing.Point(27, 21);
            this.lblBand1.Name = "lblBand1";
            this.lblBand1.Size = new System.Drawing.Size(86, 17);
            this.lblBand1.TabIndex = 0;
            this.lblBand1.Text = "选择波段1：";
            // 
            // lblBand2
            // 
            this.lblBand2.AutoSize = true;
            this.lblBand2.Location = new System.Drawing.Point(27, 66);
            this.lblBand2.Name = "lblBand2";
            this.lblBand2.Size = new System.Drawing.Size(86, 17);
            this.lblBand2.TabIndex = 1;
            this.lblBand2.Text = "选择波段2：";
            // 
            // cbxBand1
            // 
            this.cbxBand1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxBand1.FormattingEnabled = true;
            this.cbxBand1.Location = new System.Drawing.Point(119, 18);
            this.cbxBand1.Name = "cbxBand1";
            this.cbxBand1.Size = new System.Drawing.Size(121, 24);
            this.cbxBand1.TabIndex = 2;
            // 
            // cbxBand2
            // 
            this.cbxBand2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxBand2.FormattingEnabled = true;
            this.cbxBand2.Location = new System.Drawing.Point(119, 63);
            this.cbxBand2.Name = "cbxBand2";
            this.cbxBand2.Size = new System.Drawing.Size(121, 24);
            this.cbxBand2.TabIndex = 3;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(165, 149);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 30);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "确定";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblBand3
            // 
            this.lblBand3.AutoSize = true;
            this.lblBand3.Location = new System.Drawing.Point(27, 111);
            this.lblBand3.Name = "lblBand3";
            this.lblBand3.Size = new System.Drawing.Size(72, 17);
            this.lblBand3.TabIndex = 5;
            this.lblBand3.Text = "选择波段3";
            // 
            // cbxBand3
            // 
            this.cbxBand3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxBand3.FormattingEnabled = true;
            this.cbxBand3.Location = new System.Drawing.Point(119, 108);
            this.cbxBand3.Name = "cbxBand3";
            this.cbxBand3.Size = new System.Drawing.Size(121, 24);
            this.cbxBand3.TabIndex = 6;
            // 
            // Bands
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(255, 194);
            this.Controls.Add(this.cbxBand3);
            this.Controls.Add(this.lblBand3);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.cbxBand2);
            this.Controls.Add(this.cbxBand1);
            this.Controls.Add(this.lblBand2);
            this.Controls.Add(this.lblBand1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Bands";
            this.Text = "选择波段";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblBand1;
        private System.Windows.Forms.Label lblBand2;
        private System.Windows.Forms.ComboBox cbxBand1;
        private System.Windows.Forms.ComboBox cbxBand2;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label lblBand3;
        private System.Windows.Forms.ComboBox cbxBand3;
    }
}