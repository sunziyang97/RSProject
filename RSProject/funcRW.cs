﻿using System;
using System.Windows.Forms;
using System.IO;

namespace RSProject
{
    public class funcRW
    {
        public void mergedata(string[] dataPaths,ref int[,,] intArray, ref byte[,,] dataArray, ref Head head)
        {
            string[] s = dataPaths;
            dataArray = new byte[100, 1000, 1000];
            intArray = new int[10, 1000, 1000];
            for (int i = 0; i < s.Length; i++)
            {
                FileStream fs = new FileStream(s[i], FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                for (int j = 0; j < head.lines; j++)
                {
                    for (int k = 0; k < head.samples; k++)
                    {
                        byte b = br.ReadByte();
                        dataArray[i, j, k] = b;
                        intArray[i, j, k] = Convert.ToInt32(b);
                    }
                }
            }
        }

        public void read_metadata(string dataPath, ref Head head)
        {
            string s = dataPath;
            try
            {
                StreamReader sr = new StreamReader(s);
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] a = line.Split('=');
                    if (a[0].Trim() == "samples")
                    {
                        head.samples = Int32.Parse(a[1].Trim());
                    }

                    if (a[0].Trim() == "lines")
                    {
                        head.lines = Int32.Parse(a[1].Trim());
                    }

                    if (a[0].Trim() == "bands")
                    {
                        head.bands = Int32.Parse(a[1].Trim());
                    }

                    if (a[0].Trim() == "header offset")
                        head.header_offset = Int32.Parse(a[1].Trim());

                    if (a[0].Trim() == "file type")
                        head.file_type = a[1].Trim();

                    if (a[0].Trim() == "data type")
                        head.data_type = Int32.Parse(a[1].Trim());

                    if (a[0].Trim() == "interleave")
                        head.interleave = a[1].Trim();
                }
                sr.Close();
            }
            catch
            {
                MessageBox.Show("Error!");
            }
        }

        public void read_data(string dataPath,ref int[,,] intArray,ref byte[,,] dataArray,ref Head head)
        {
            FileStream fs = new FileStream(dataPath, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            dataArray = new byte[100, 1000, 1000];
            intArray = new int[10, 1000, 1000];
            if (head.interleave == "bsq")
            {
                for (int i = 0; i < head.bands; i++)
                {
                    for (int j = 0; j < head.lines; j++)
                    {
                        for (int k = 0; k < head.samples; k++)
                        {
                            byte b = br.ReadByte();
                            dataArray[i, j, k] = b;
                            intArray[i, j, k] = Convert.ToInt32(b);
                        }
                    }
                }
            }
            if (Global.head.interleave == "bil")
            {
                for (int j = 0; j < Global.head.lines; j++)
                {
                    for (int i = 0; i < Global.head.bands; i++)
                    {
                        for (int k = 0; k < Global.head.samples; k++)
                        {
                            byte b = br.ReadByte();
                            Global.dataArray[i, j, k] = b;
                            Global.intArray[i, j, k] = Convert.ToInt32(b);
                        }
                    }
                }
            }
            if (Global.head.interleave == "bip")
            {
                for (int j = 0; j < Global.head.lines; j++)
                {
                    for (int k = 0; k < Global.head.samples; k++)
                    {
                        for (int i = 0; i < Global.head.bands; i++)
                        {
                            byte b = br.ReadByte();
                            Global.dataArray[i, j, k] = b;
                            Global.intArray[i, j, k] = Convert.ToInt32(b);
                        }
                    }
                }
            }
            br.Close();
        }

        public void write_metadata(string dataPath, string leave, Head head)
        {
            string s = dataPath;
            StreamWriter sw = new StreamWriter(s);
            sw.WriteLine("ENVI");
            sw.WriteLine("samples = " + head.samples);
            sw.WriteLine("lines = " + head.lines);
            sw.WriteLine("bands = " + head.bands);
            sw.WriteLine("header offset = " + head.header_offset);
            sw.WriteLine("file type = " + head.file_type);
            sw.WriteLine("data type = " + head.data_type);
            sw.WriteLine("interleave = " + leave);
            sw.Close();
        }

        public void ToBSQ(string P, Head head, byte[,,] dataArray)
        {
            string s_data = P;
            string s_metadata = P + ".hdr";
            write_metadata(s_metadata, "bsq", head);
            BinaryWriter bw = new BinaryWriter(File.Open(s_data, FileMode.Create));
            for (int i = 0; i < head.bands; i++)
            {
                for (int j = 0; j < head.lines; j++)
                {
                    for (int k = 0; k < head.samples; k++)
                        bw.Write(dataArray[i, j, k]);
                }
            }
            bw.Close();
            MessageBox.Show("成功！");
        }

        public void ToBIL(string P, Head head, byte[,,] dataArray)
        {
            string s_data = P;
            string s_metadata = P + ".hdr";
            write_metadata(s_metadata, "bil", head);
            BinaryWriter bw = new BinaryWriter(File.Open(s_data, FileMode.Create));
            for (int j = 0; j < head.lines; j++)
            {
                for (int i = 0; i < head.bands; i++)
                {
                    for (int k = 0; k < head.samples; k++)
                        bw.Write(dataArray[i, j, k]);
                }
            }
            bw.Close();
            MessageBox.Show("成功！");
        }

        public void ToBIP(string P, Head head, byte[,,] dataArray)
        {
            string s_data = P;
            string s_metadata = P + ".hdr";
            write_metadata(s_metadata, "bip", head);
            BinaryWriter bw = new BinaryWriter(File.Open(s_data, FileMode.Create));
            for (int j = 0; j < head.lines; j++)
            {
                for (int k = 0; k < head.samples; k++)
                {
                    for (int i = 0; i < head.bands; i++)
                        bw.Write(dataArray[i, j, k]);
                }
            }
            bw.Close();
            MessageBox.Show("成功！");
        }
    }
}
