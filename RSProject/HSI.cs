﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSProject
{
    public sealed class HSI
    {
        public double Hue;
        public double Saturation;
        public double Intensity;

        public HSI() { }
        public HSI(int hue, double saturation, double intensity)
        {
            Hue = hue;
            Saturation = saturation;
            Intensity = intensity;
        }

        public override string ToString()
        {
            return String.Format("HSI: ({0},{1},{2})", Hue, Saturation, Intensity);
        }
    }
}
