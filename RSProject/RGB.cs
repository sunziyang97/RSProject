﻿using System;
using System.Drawing;

namespace RSProject
{
    public sealed class RGB
    {
        public byte Red;
        public byte Green;
        public byte Blue;

        public short R = 2;
        public short G = 1;
        public short B = 0;

        public RGB() { }

        public RGB(byte red,byte green,byte blue)
        {
            Red = red;
            Green = green;
            Blue = blue;
        }

        public RGB(Color color)
        {
            Red = color.R;
            Green = color.G;
            Blue = color.B;
        }

        public Color color
        {
            get { return Color.FromArgb(Red, Green, Blue); }
            set
            {
                Red = value.R;
                Green = value.G;
                Blue = value.B;
            }
        }

        public override string ToString()
        {
            return String.Format("RGB:({0},{1},{2})", Red, Green, Blue);
        }
    }
}
